package com.conferences.sciezor.conferences;

import com.conferences.sciezor.conferences.controller.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class ControllerTest {
    @Autowired
    private AppController appController;

    @Test
    public void appControllerLoads() throws Exception{
        assertThat(appController).isNotNull();
    }

    @Autowired
    private LoginController loginController;

    @Test
    public void loginControllerLoads() throws Exception{
        assertThat(loginController).isNotNull();
    }

    @Autowired
    private ModeratorController moderatorController;

    @Test
    public void moderatorControllerLoads() throws Exception{
        assertThat(moderatorController).isNotNull();
    }

    @Autowired
    private SpeakerController speakerController;

    @Test
    public void speakerControllerLoads() throws Exception{
        assertThat(speakerController).isNotNull();
    }

    @Autowired
    private UserController userController;

    @Test
    public void userControllerLoads() throws Exception{
        assertThat(userController).isNotNull();
    }

}
