package com.conferences.sciezor.conferences;

import com.conferences.sciezor.conferences.dto.UserDTO;
import com.conferences.sciezor.conferences.entity.User;
import com.conferences.sciezor.conferences.repository.UserRepository;
import com.conferences.sciezor.conferences.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Assert;
import org.springframework.web.context.WebApplicationContext;

import java.util.function.BooleanSupplier;

import static java.lang.reflect.Array.get;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
public class RegisterTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WebApplicationContext applicationContext;
    private MockMvc mvc;

    @BeforeAll
    public void init(){
        this.mvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();

        setUpUser();
    }

    private void setUpUser(){
        User user = new User();
        user.setIduser(1);
        user.setUsername("Irmina1");
        user.setPassword("password1234");
        user.setCity("Krakow");
        user.setEmail("irmina@gmail.com");
        user.setName("Irmina");
        user.setPhone("111222334");
        user.setSurname("Sciezor");
        userRepository.save(user);
    }

//    @Test
//    @WithUserDetails("irmina@gmail.com")
//    public void getUserFromContext() throws Exception {
//        mvc.perform(get("/user/userInContext"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType("application/json"))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value("Sidney"))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("sidney@gmail.com"))
//                .andDo(print());
//    }
}
