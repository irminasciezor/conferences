package com.conferences.sciezor.conferences;
import com.conferences.sciezor.conferences.entity.User;
import com.conferences.sciezor.conferences.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
public class UserRepositoryTest {
    @Autowired
    private TestEntityManager testEntityManager;
    @Autowired
    private UserRepository repository;

    @Test
    public void testCreateUser(){
        User user = new User();
        user.setIduser(1);
        user.setUsername("Irmina1");
        user.setPassword("password1234");
        user.setCity("Krakow");
        user.setEmail("irmina@gmail.com");
        user.setName("Irmina");
        user.setPhone("111222334");
        user.setSurname("Sciezor");

        User savedUser = repository.save(user);

        User existUser = testEntityManager.find(User.class, savedUser.getIduser());

        assertThat(user.getUsername().equals(existUser.getUsername()));

    }

}
