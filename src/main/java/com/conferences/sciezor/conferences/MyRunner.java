package com.conferences.sciezor.conferences;

import com.conferences.sciezor.conferences.entity.Report;
import com.conferences.sciezor.conferences.entity.User;
import com.conferences.sciezor.conferences.repository.ReportRepository;
import com.conferences.sciezor.conferences.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MyRunner implements CommandLineRunner {
    private final UserRepository userRepository;
    private final ReportRepository repository;

    public MyRunner(UserRepository userRepository, ReportRepository repository) {
        this.userRepository = userRepository;
        this.repository = repository;
    }

    @Override
    public void run(String... args) throws Exception {
//        userRepository.save(new User());
//        Report report = new Report();
//        report.setTopic("Palms");
//        repository.save(report);
    }
}
