package com.conferences.sciezor.conferences.entity;

import javax.persistence.*;

@Entity
@Table(name = "register")
public class Register {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idregistration", nullable = false)
    private Long idregistration;

    @Column(name = "user_email")
    String userEmail;

    @Column(name = "event_name")
    String eventName;

    public Register() {
    }

    public Long getIdregistration() {
        return idregistration;
    }

    public void setIdregistration(Long idregistration) {
        this.idregistration = idregistration;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }
}
