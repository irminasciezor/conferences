package com.conferences.sciezor.conferences.entity;

import javax.persistence.*;

@Entity
@Table(name = "speaker_topics")
public class Speaker {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idspeaker_topics", nullable = false)
    private Integer idspeaker_topics;

    @Column(name = "email_of_the_speaker")
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTopic_for_speaker() {
        return topic_for_speaker;
    }

    public void setTopic_for_speaker(String topic_for_speaker) {
        this.topic_for_speaker = topic_for_speaker;
    }

    @Column(name = "suggested_topic")
    private String topic_for_speaker;

    public Integer getIdspeaker_topics() {
        return idspeaker_topics;
    }

    public void setIdspeaker_topics(Integer idspeaker_topics) {
        this.idspeaker_topics = idspeaker_topics;
    }
}
