package com.conferences.sciezor.conferences.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "roles")
public class Roles {
    @Id
    @Column(name = "idroles", nullable = false)
    private Long idroles;

    @ManyToMany(mappedBy = "users_roles")
    private Set<User> users;

    @Column(name = "nameofrole")
    private String nameOfRole;

    public void setNameOfRole(String nameOfRole) {
        this.nameOfRole = nameOfRole;
    }

    public String getNameOfRole() {
        return nameOfRole;
    }

    public Long getIdroles() {
        return idroles;
    }

    public void setIdroles(Long idroles) {
        this.idroles = idroles;
    }

}
