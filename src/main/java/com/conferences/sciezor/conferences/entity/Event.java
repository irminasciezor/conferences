package com.conferences.sciezor.conferences.entity;

import javax.persistence.*;
import java.sql.Time;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "event")
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idevent", nullable = false)
    private Long idevent;

    @Column(name = "name")
    private String name;
    @Column(name = "date")
    private LocalDate date;
    @Column(name = "participants")
    private int participants;
    @Column(name = "reports")
    private int reports;
    @Column(name = "place")
    private String place;
    @Column(name = "time")
    private Time time;

    @Column(name = "actual_participants")
    private Integer actual;
    @ManyToOne
    @JoinColumn(name = "user_iduser")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Event() {
    }

    public void setParticipants(int participants) {
        this.participants = participants;
    }

    public Integer getActual() {
        return actual;
    }

    public void setActual(Integer actual) {
        this.actual = actual;
    }

    public Long getIdevent() {
        return idevent;
    }

    public void setIdevent(Long idevent) {
        this.idevent = idevent;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setParticipants(Integer participants) {
        this.participants = participants;
    }

    public void setReports(Integer reports) {
        this.reports = reports;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public LocalDate getDate() {
        return date;
    }

    public Integer getParticipants() {
        return participants;
    }

    public Integer getReports() {
        return reports;
    }

    public String getPlace() {
        return place;
    }

    public Time getTime() {
        return time;
    }

}
