package com.conferences.sciezor.conferences.entity;
import javax.persistence.*;

@Entity
@Table(name = "report")
public class Report {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idreport", nullable = false)
    private Long idreport;

    @Column(name = "topic")
    private String topic;

    public Report() {
    }

    public Long getIdreport() {
        return idreport;
    }

    public void setIdreport(Long idreport) {
        this.idreport = idreport;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTopic() {
        return topic;
    }
}
