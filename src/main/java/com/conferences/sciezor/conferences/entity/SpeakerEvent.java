package com.conferences.sciezor.conferences.entity;

import javax.persistence.*;

@Entity
@Table(name = "speaker_events")
public class SpeakerEvent {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idspeaker_events", nullable = false)
    private Long idspeaker_events;

    @Column(name = "name_of_speaker")
    private String nameOfSpeaker;

    @Column(name = "name_of_event")
    private String nameOfEvent;

    @Column(name = "topic_of_report")
    private String topicOfReport;

    public String getNameOfSpeaker() {
        return nameOfSpeaker;
    }

    public void setNameOfSpeaker(String nameOfSpeaker) {
        this.nameOfSpeaker = nameOfSpeaker;
    }

    public String getNameOfEvent() {
        return nameOfEvent;
    }

    public void setNameOfEvent(String nameOfEvent) {
        this.nameOfEvent = nameOfEvent;
    }

    public String getTopicOfReport() {
        return topicOfReport;
    }

    public void setTopicOfReport(String topicOfReport) {
        this.topicOfReport = topicOfReport;
    }

    public Long getIdspeaker_events() {
        return idspeaker_events;
    }

    public void setIdspeaker_events(Long idspeaker_events) {
        this.idspeaker_events = idspeaker_events;
    }
}
