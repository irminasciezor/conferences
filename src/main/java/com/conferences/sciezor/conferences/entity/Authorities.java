package com.conferences.sciezor.conferences.entity;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "authorities")
public class Authorities {
    @Column(name = "username")
    private String username;
    @Column(name = "authority")
    private String authority;
    @Column(name = "id")
    private int id;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
