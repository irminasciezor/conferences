package com.conferences.sciezor.conferences.service;
import com.conferences.sciezor.conferences.entity.Event;
import java.sql.Time;

public interface ModeratorService {

    void createEvent();

    void createTopicForReport(String topic);

    void regulateEvent(Event event, Time time, String place);
}
