package com.conferences.sciezor.conferences.service.impl;
import com.conferences.sciezor.conferences.entity.Speaker;
import com.conferences.sciezor.conferences.entity.SpeakerEvent;
import com.conferences.sciezor.conferences.entity.User;
import com.conferences.sciezor.conferences.repository.SpeakerEventRepository;
import com.conferences.sciezor.conferences.repository.SpeakerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class SpeakerServiceImpl{
    @Autowired
    SpeakerRepository speakerRepository;

    @Autowired
    SpeakerEventRepository speakerEventRepository;


    public void save(Speaker speaker){
        speakerRepository.save(speaker);
    }

    public List<Speaker> showList(){
        return speakerRepository.findAll();
    }



    public List<String> findAllFilter(String email){
        List<Speaker> list = showList();
        List<String> stringList = new ArrayList<>();
        for (Speaker e:list)
        {
            if (Objects.equals(e.getEmail(), email)){
                if (speakerRepository.findTopic(e.getIdspeaker_topics())!= null)
                {
                    stringList.add(speakerRepository.findTopic(e.getIdspeaker_topics()));
                }
            }
        }
        return stringList;
    }



}
