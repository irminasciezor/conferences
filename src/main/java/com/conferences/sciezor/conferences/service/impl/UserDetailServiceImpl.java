package com.conferences.sciezor.conferences.service.impl;
import com.conferences.sciezor.conferences.entity.User;
import com.conferences.sciezor.conferences.repository.UserRepository;
import com.conferences.sciezor.conferences.service.exception.IllegalUsernameException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws IllegalUsernameException {
        final User user = userRepository.findByEmail(username);
        if(user==null){
            throw new UsernameNotFoundException(username);
        }
        return new MyUserPrincipal(user);
    }

}
