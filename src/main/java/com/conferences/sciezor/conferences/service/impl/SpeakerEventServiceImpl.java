package com.conferences.sciezor.conferences.service.impl;

import com.conferences.sciezor.conferences.entity.SpeakerEvent;
import com.conferences.sciezor.conferences.repository.SpeakerEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class SpeakerEventServiceImpl {

    @Autowired
    SpeakerEventRepository speakerEventRepository;

    public void save(SpeakerEvent speakerEvent){
        speakerEventRepository.save(speakerEvent);
    }

    public List<SpeakerEvent> findAllSuggestions(){
        return speakerEventRepository.findAll();
    }
    public List<SpeakerEvent> findAllEventsFilter(String username){
        List<SpeakerEvent> list = showListOfEvents();
        List<SpeakerEvent> stringList = new ArrayList<>();
        for(SpeakerEvent s:list){
            if (Objects.equals(s.getNameOfSpeaker(), username)){
                stringList.add(s);
            }
        }
        return stringList;
    }
    public List<SpeakerEvent> showListOfEvents(){
        return speakerEventRepository.findAll();
    }


}
