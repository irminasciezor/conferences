package com.conferences.sciezor.conferences.service;

public interface PasswordEncoder {
    String encode(String password);

    boolean isMatching(String originalPassword, String encodedPassword);
}
