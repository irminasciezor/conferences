package com.conferences.sciezor.conferences.service.impl;
import com.conferences.sciezor.conferences.dto.UserDTO;
import com.conferences.sciezor.conferences.entity.Roles;
import com.conferences.sciezor.conferences.entity.User;
import com.conferences.sciezor.conferences.entity.Users;
import com.conferences.sciezor.conferences.repository.RoleRepository;
import com.conferences.sciezor.conferences.repository.UserRepository;
import com.conferences.sciezor.conferences.service.UserService;
import com.conferences.sciezor.conferences.service.exception.UserAlreadyExistException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private final UserRepository userRepository;


    private BCryptPasswordEncoder passwordEncoder;
    @Transactional
    public User save(User user){
        return userRepository.save(user);
    }


    private final ModelMapper modelMapper;

    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    @Transactional
    public User register(UserDTO userDto) throws UserAlreadyExistException {
//        if (emailExists(userDto.getEmail())) {
//            throw new UserAlreadyExistException("There is an account with that email address: "
//                    + userDto.getEmail());
//        }

        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setName(userDto.getName());
        user.setSurname(userDto.getSurname());
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());
//        modelMapper.map(userDto,user);


        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        return save(user);
    }

    @Override
    public User login(Users users) {
        User user = new User(users.getUsername(), users.getPassword());
        return save(user);
    }


    public void addToUsers(String username, String password){
        Users users = new Users();
        User user = new User(users.getUsername(),users.getPassword());
        userRepository.save(user);
    }

    public List<User> getAllUsers(){
        User user = new User();
        List<User> allUsers = new ArrayList<>();
        allUsers.add(userRepository.findByEmail(user.getEmail()));
        return allUsers;

    }

    public User getUser(String username){
        User user = new User();
        user = userRepository.findByUsername(username);
        if (user == null)
            return null;
        return user;
    }

}
