package com.conferences.sciezor.conferences.service;

import com.conferences.sciezor.conferences.entity.Event;

import java.util.List;

public interface SpeakerService {

    List<Event> registerForEvent(Event event);
}
