package com.conferences.sciezor.conferences.service.exception;

public class IllegalPasswordException extends RuntimeException{
    public IllegalPasswordException(String message){
        super(message);
    }
}
