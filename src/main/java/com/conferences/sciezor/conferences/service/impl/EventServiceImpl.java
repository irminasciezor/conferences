package com.conferences.sciezor.conferences.service.impl;

import com.conferences.sciezor.conferences.controller.EventController;
import com.conferences.sciezor.conferences.dto.EventDTO;
import com.conferences.sciezor.conferences.dto.UserDTO;
import com.conferences.sciezor.conferences.entity.Event;
import com.conferences.sciezor.conferences.entity.Register;
import com.conferences.sciezor.conferences.repository.EventRepository;
import com.conferences.sciezor.conferences.repository.RegisterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class EventServiceImpl {

    private static final Logger log = LoggerFactory.getLogger(EventServiceImpl.class);

    @Autowired
    EventRepository eventRepository;

    @Autowired
    RegisterRepository registerRepository;


    public void updateStatistics(Integer currentValue, Integer idevent){
        Event event = eventRepository.findById(Long.valueOf(idevent)).get();
        event.setActual(currentValue);
        save(event);
    }

    public List<Event> findAllEvents() {
        log.info(eventRepository.findAll().toString());
        return (List<Event>) eventRepository.findAll();
    }

    public List<Integer> findSumOfParticipants(){
        List<Event> list = eventRepository.findAllEvents();
        List<Integer> integers = new ArrayList<>();
        for (int i=0; i<100; i++){
            integers.add(0);
        }
        for(Event e:list){
            log.info("idevent: " + e.getIdevent() + " " + e.getParticipants());
            integers.add(Math.toIntExact(e.getIdevent()),e.getParticipants());
        }
        return integers;
    }

    public List<Event> findAllByDateASC(){
        return (List<Event>)eventRepository.findAll(Sort.by(Sort.Direction.ASC, "date"));
    }

    public List<Event> findAllByDateDESC(){
        return eventRepository.findAll(Sort.by(Sort.Direction.DESC, "date"));
    }

    public void changeEventDetails(String name, String place, Integer id, String date, LocalTime time){
        Event event = eventRepository.findById(Long.valueOf(id)).get();
        event.setName(name);
        event.setPlace(place);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(date, formatter);
        event.setDate(localDate);
        event.setTime(Time.valueOf(time));
        eventRepository.save(event);
    }

    public void DeleteEvent(Integer id){
        eventRepository.deleteById(Long.valueOf(id));
    }

    public boolean changeEvent(String name, String date, String place, String time){
        Event event = eventRepository.findByName(name);
        if (event == null)
            return false;
        CharSequence sequence = time;
        String s = sequence.toString();
        event.setPlace(place);
        event.setDate(LocalDate.parse(date));
        event.setTime(Time.valueOf(LocalTime.parse(s)));
        eventRepository.save(event);
        return true;
    }

    public List<Event> saveAll(List<Event> eventList){
        return eventRepository.saveAll(eventList);
    }

    public void save(Event event){
        eventRepository.save(event);
    }

    @Transactional
    public Register signUpForEvent(String name, String email){
        Register register = new Register();
        EventDTO eventDTO = new EventDTO();
        Event event = eventRepository.findByName(name);
        event.setParticipants(event.getParticipants() + 1);
        name = eventDTO.getName();
        UserDTO userDTO = new UserDTO();
        email = userDTO.getEmail();
        register.setEventName(name);
        register.setUserEmail(email);
        eventRepository.save(event);
        return registerRepository.save(register);

    }


}
