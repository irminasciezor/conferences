package com.conferences.sciezor.conferences.service;

public interface ValidatorService {

    boolean validatePassword();

    void checkPassword(String password);

    boolean checkUsername(String username);

    void checkPhoneNumber(String phone);

    boolean checkEmail(String email);
}
