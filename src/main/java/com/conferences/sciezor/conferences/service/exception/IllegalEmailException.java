package com.conferences.sciezor.conferences.service.exception;

public class IllegalEmailException extends RuntimeException{
    public IllegalEmailException(String message){
        super(message);
    }
}
