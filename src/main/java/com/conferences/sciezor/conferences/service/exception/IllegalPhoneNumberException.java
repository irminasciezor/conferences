package com.conferences.sciezor.conferences.service.exception;

public class IllegalPhoneNumberException extends RuntimeException{
    public IllegalPhoneNumberException(String message){
        super(message);
    }
}
