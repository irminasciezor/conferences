package com.conferences.sciezor.conferences.service;

public interface SecurityService {
    boolean isAuthenticated();
    void autoLogin(String username, String password);
}
