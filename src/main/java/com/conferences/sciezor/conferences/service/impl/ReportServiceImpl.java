package com.conferences.sciezor.conferences.service.impl;
import com.conferences.sciezor.conferences.entity.Report;
import com.conferences.sciezor.conferences.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportServiceImpl {

    @Autowired
    ReportRepository reportRepository;

    public void save(Report report){
        reportRepository.save(report);
    }

    public List<Report> findAllTopics(){
        return reportRepository.findAll();
    }
}
