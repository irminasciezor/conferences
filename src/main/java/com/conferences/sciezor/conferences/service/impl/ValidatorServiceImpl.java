//package com.conferences.sciezor.conferences.service.impl;
//
//import com.conferences.sciezor.conferences.reguest.UserRegistrationRequest;
//import com.conferences.sciezor.conferences.service.ValidatorService;
//import com.conferences.sciezor.conferences.service.exception.IllegalEmailException;
//import com.conferences.sciezor.conferences.service.exception.IllegalPasswordException;
//import com.conferences.sciezor.conferences.service.exception.IllegalPhoneNumberException;
//import com.conferences.sciezor.conferences.service.exception.IllegalUsernameException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//public class ValidatorServiceImpl implements ValidatorService {
//    Logger logger = LoggerFactory.getLogger("Validation");
//
//    Pattern pattern;
//
//    Matcher matcher;
//
//    //(?=.*\\d) – at least 1 digit.
//    //(?=\\S+$) – no white-space
//    //(?=.*[@#$%^&+=]) – at least one special symbol (only the ones given within the [ ]).
//    //(?=.*[a-z]) -at least one lower case letter.
//    //(?=.*[A-Z]) – at least one upper case letter.
//    //.{8,10} – minimum 8 and, maximum of 10 letters (can change to any convenient length).
//    String passwordRegex = "^(?=.*\\d)(?=\\S+$)(?=.*[@#$%^&+=])(?=.*[a-z])(?=.*[A-Z]).{8,10}$";
//
//    //The username length must range between 7 to 20 otherwise, it’s an invalid username.
//    //The username is allowed to contain only underscores ( _ ) other than alphanumeric characters.
//    //The first character of the username must be an alphabetic character, i.e., [a-z] or [A-Z].
//    String usernameRegex = "^[a-zA-Z][a-zA-Z0-9_]{6,19}$";
//
//    // allow an international prefix at the start of a phone number
//    String phoneRegex = "^(\\+\\d{1,3}( )?)?((\\(\\d{3}\\))|\\d{3})[- .]?\\d{3}[- .]?\\d{4}$";
//
//    String emailRegex = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";
//
//    @Override
//    public boolean validatePassword() {
//        logger.info("Password 2 validation");
//        return userRegistrationRequest.getPassword().equals(userRegistrationRequest.getRepeatPassword());
//    }
//
//    @Override
//    public void checkPassword(String password) {
//        logger.info("Password validation");
//        password = userRegistrationRequest.getPassword();
//        pattern = Pattern.compile(passwordRegex);
//        matcher = pattern.matcher(password);
//        if(!matcher.matches())
//            throw new IllegalPasswordException("Invalid password!");
//
//
//    }
//
//    @Override
//    public boolean checkUsername(String username) {
//        logger.info("Username validation");
//        pattern = Pattern.compile(usernameRegex);
//        matcher = pattern.matcher(username);
//        return matcher.matches();
//
//    }
//
//    @Override
//    public void checkPhoneNumber(String phone) {
//        logger.info("Phone validation");
//        phone = userRegistrationRequest.getPhone();
//        pattern = Pattern.compile(phoneRegex);
//        matcher = pattern.matcher(phone);
//        if(!matcher.matches())
//            throw new IllegalPhoneNumberException("Invalid phone number!");
//
//
//    }
//
//    @Override
//    public boolean checkEmail(String email) {
//        logger.info("Email validation");
//        if(email == null)
//            return false;
//        pattern = Pattern.compile(emailRegex);
//        matcher = pattern.matcher(email);
//        return matcher.matches();
//
//    }
//}
