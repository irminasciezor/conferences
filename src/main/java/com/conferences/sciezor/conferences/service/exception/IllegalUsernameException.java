package com.conferences.sciezor.conferences.service.exception;

public class IllegalUsernameException extends RuntimeException{
    public IllegalUsernameException(String message){
        super(message);
    }
}
