package com.conferences.sciezor.conferences.service;
import com.conferences.sciezor.conferences.dto.UserDTO;
import com.conferences.sciezor.conferences.entity.User;
import com.conferences.sciezor.conferences.entity.Users;
import com.conferences.sciezor.conferences.service.exception.UserAlreadyExistException;

import java.util.ArrayList;
import java.util.List;

public interface UserService {
    User register(UserDTO userDto) throws UserAlreadyExistException;

    User login(Users users);
}
