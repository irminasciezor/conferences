package com.conferences.sciezor.conferences.repository;

import com.conferences.sciezor.conferences.entity.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Roles, Long> {
    Roles save(Roles roles);

//    @Query("select r from Roles r where r.nameOfRole is not null")
//    List<Roles> findAll();
    @Modifying
    @Transactional
    @Query(value = "INSERT INTO roles(idroles,nameofrole) VALUES(1,'MODERATOR'),(2,'SPEAKER'),(3,'GUEST')",nativeQuery = true)
    void saveRoles();

    @Override
    List<Roles> findAll();
}
