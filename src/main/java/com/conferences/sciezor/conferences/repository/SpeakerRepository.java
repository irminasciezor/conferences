package com.conferences.sciezor.conferences.repository;

import com.conferences.sciezor.conferences.entity.Speaker;
import com.conferences.sciezor.conferences.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SpeakerRepository extends JpaRepository<Speaker, Long> {
    Speaker save(Speaker speaker);

    @Query(value = "select a from Speaker a where email:email", nativeQuery = true)
    List<User> findAllByEmail(String email);

    @Query(value = "select suggested_topic from speaker_topics where idspeaker_topics = :id", nativeQuery = true)
    String findTopic(Integer id);
}
