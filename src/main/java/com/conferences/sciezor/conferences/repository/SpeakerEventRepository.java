package com.conferences.sciezor.conferences.repository;

import com.conferences.sciezor.conferences.entity.SpeakerEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SpeakerEventRepository extends JpaRepository<SpeakerEvent, Long> {
    SpeakerEvent save(SpeakerEvent speakerEvent);

    @Query(value = "select name_of_event, topic_of_report from speaker_events where name_of_speaker = :username", nativeQuery = true)
    String findEvent(String username);
}
