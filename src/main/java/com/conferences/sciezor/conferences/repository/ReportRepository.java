package com.conferences.sciezor.conferences.repository;

import com.conferences.sciezor.conferences.entity.Report;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportRepository extends JpaRepository<Report, Long> {
    Report save(Report report);
}
