package com.conferences.sciezor.conferences.repository;
import com.conferences.sciezor.conferences.dto.UserDTO;
import com.conferences.sciezor.conferences.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);

    User findByUsername(String username);

}
