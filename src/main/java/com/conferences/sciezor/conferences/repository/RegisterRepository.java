package com.conferences.sciezor.conferences.repository;

import com.conferences.sciezor.conferences.entity.Register;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RegisterRepository extends JpaRepository<Register, Long> {
    Register save(Register register);

    @Query(value = "SELECT COUNT(idregistration) as NumberOfUsers FROM register", nativeQuery = true)
    long count();
}
