package com.conferences.sciezor.conferences.repository;

import com.conferences.sciezor.conferences.entity.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
    Event save(Event event);

    @Query(value = "select date from event e where e.date like %:date%", nativeQuery = true)
    List<Event> findByDate(@Param("date") LocalDate date);

    Event findByName(String name);

    @Query(value = "select * from event", nativeQuery = true)
    List<Event> findAllEvents();

    @Query(value = "select count(user_email) from register where event_name = :eventname", nativeQuery = true)
    Integer getSumOfName(String eventname);

}
