package com.conferences.sciezor.conferences.repository;

import com.conferences.sciezor.conferences.entity.Event;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;
@Repository
public interface PageRepository extends PagingAndSortingRepository<Event, Long> {
//    List<Event> findAllById(long id, Pageable pageable);
}
