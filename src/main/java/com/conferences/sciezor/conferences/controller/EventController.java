package com.conferences.sciezor.conferences.controller;
import com.conferences.sciezor.conferences.dto.EventDTO;
import com.conferences.sciezor.conferences.entity.Event;
import com.conferences.sciezor.conferences.repository.EventRepository;
import com.conferences.sciezor.conferences.repository.RegisterRepository;
import com.conferences.sciezor.conferences.service.impl.EventServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Controller
public class EventController {

    @Autowired
    EventServiceImpl eventService;

    @Autowired
    RegisterRepository registerRepository;

    @Autowired
    EventRepository eventRepository;

    private static final Logger log = LoggerFactory.getLogger(EventController.class);

    @GetMapping("/register_for_event")
    public String showAllEvents(Model model){

        model.addAttribute("events", eventService.findAllEvents());
        boolean myBooleanVariable = false;
        model.addAttribute("myBooleanVariable", myBooleanVariable);
        boolean checkEmail = false;
        model.addAttribute("checkEmail", checkEmail);

        log.info(eventService.findAllEvents().toString());

        return "register_for_event";
    }

    @GetMapping("/moderator_account")
    public String showCreateForm(Model model, HttpServletRequest request) {

        model.addAttribute("EventDTO", new EventDTO());
        model.addAttribute("events", eventService.findAllEvents());

        final String currentUsername = SecurityContextHolder.getContext().getAuthentication().getName();

        log.info("current login:" + currentUsername);

        int page = 0; //default page number is 0 (yes it is weird)
        int size = 1; //default page size is 10

        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
            page = Integer.parseInt(request.getParameter("page")) - 1;
        }

        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
            size = Integer.parseInt(request.getParameter("size"));
        }

        model.addAttribute("customers", eventRepository.findAll(PageRequest.of(page, size)));

        return "moderator_account";
    }

    @PostMapping("/moderator_account")
    public String saveEvents(@RequestParam(name = "name", required = false) String name,
                            @RequestParam(name = "place", required = false) String place,
                             @RequestParam(name = "date", required = false) String date,
                             @RequestParam(name = "time", required = false) LocalTime time){

        log.info("Name of Event: {}, Place: {}, Date: {}, Time: {}", name, place, date, time);

        Event event = new Event();
        event.setName(name);
        event.setPlace(place);
        event.setDate(LocalDate.parse(date));
        event.setTime(Time.valueOf(time));
        eventService.save(event);

        return "moderator_account";
    }

    @PostMapping("/updateEvent")
    public String updateEvent(@RequestParam("idevent") Long idevent, Model model){

        log.info("Id event: {}", idevent);

        model.addAttribute("idevent", idevent);

        return "edit";
    }

    @PostMapping("/updateEventDetails")
    public String updateEventDetails(@RequestParam("name") String name,
                                     @RequestParam("place") String place,
                                     @RequestParam("date") String date,
                                     @RequestParam("time") LocalTime time,
                                     @RequestParam("idevent") Integer idevent){

        log.info("name: {}, place: {}, date: {}, time: {}, event id: {}",name,place,date,time,idevent);

        eventService.changeEventDetails(name,place,idevent,date,time);

        return "redirect:/moderator_account";
    }

@PostMapping("/delete_event")
public String deleteEvent(@RequestParam("idevent") Integer idevent){

        eventService.DeleteEvent(idevent);

        return "redirect:/moderator_account";
    }

    @Transactional
    @PostMapping("/change_event")
    public String changeEvent(@RequestParam(name = "nameOfEvent") String nameOfEvent,
                              @RequestParam(name = "newDate")String newDate,
                              @RequestParam(name = "newPlace")String newPlace,
                              @RequestParam(name = "newTime")String newTime){

        log.info("Name of Event: {}, New Date: {}, New Place: {}, New Time: {}",nameOfEvent,newDate,newPlace,newTime);

        eventService.changeEvent(nameOfEvent, newDate, newPlace, newTime);

        return "moderator_account";
    }

    @PostMapping("/register_for_event")
    public String signUpEvent(@RequestParam(name = "eventName")String name,
                              @RequestParam(name = "userEmail")String email){

        log.info("Name of Event: {}, User email: {}",name, email);

        eventService.signUpForEvent(name,email);

        return "register_for_event";
    }

    @PostMapping("/submitCurrentValue")
    public String submitCurrentValue(@RequestParam(name = "currentValue") String currentValue,
                                     @RequestParam(name = "idevent") String idevent){

        eventService.updateStatistics(Integer.valueOf(currentValue), Integer.valueOf(idevent));

        return "redirect:/statistics";
    }

    @GetMapping("/statistics")
    public String statistics(Model model){

        log.info(eventService.findSumOfParticipants().toString());

        model.addAttribute("events", eventService.findAllEvents());
        model.addAttribute("part",eventService.findSumOfParticipants());

        return "statistics";
    }

    @PostMapping("/statistics")
    public String viewStatistics(){
        return "statistics";
    }

    @PostMapping("/sortByDateASC")
    public String sortByDateASC(Model model){

        log.info("Sort date ASC : ");

        List<Event> event = eventService.findAllByDateASC();
        model.addAttribute("events", event);

        return "moderator_account";
    }

    @PostMapping("sortByDateDESC")
    public String sortByDateDESC(Model model){

        log.info("Sort by date DESC");

        List<Event> events = eventService.findAllByDateDESC();
        model.addAttribute("events", events);

        return "moderator_account";
    }
}
