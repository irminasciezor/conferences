package com.conferences.sciezor.conferences.controller;

import com.conferences.sciezor.conferences.entity.Event;
import com.conferences.sciezor.conferences.entity.Report;
import com.conferences.sciezor.conferences.entity.Speaker;
import com.conferences.sciezor.conferences.entity.User;
import com.conferences.sciezor.conferences.repository.SpeakerRepository;
import com.conferences.sciezor.conferences.service.impl.ReportServiceImpl;
import com.conferences.sciezor.conferences.service.impl.SpeakerServiceImpl;
import com.conferences.sciezor.conferences.service.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ReportController {

    @Autowired
    ReportServiceImpl reportService;
    @Autowired
    SpeakerServiceImpl speakerService;

    @Autowired
    UserServiceImpl userService;

    private static final Logger log = LoggerFactory.getLogger(ReportController.class);

    @GetMapping("/create_report")
    public String createReport(Model model){
        return "moderator_account";
    }

    @PostMapping("/save_report")
    public String saveReport(@RequestParam(name = "topic")String topic){

        Report report = new Report();
        report.setTopic(topic);

        log.info("topic: " + topic);

        reportService.save(report);

        return "moderator_account";
    }

    @GetMapping("/suggested_topics")
    public String showAllTopicsForSpeakers(Model model){

        final String currentLogin = SecurityContextHolder.getContext().getAuthentication().getName();

        User user = userService.getUser(currentLogin);
        if (user != null) {
            String email = user.getEmail();
            model.addAttribute("topics", speakerService.findAllFilter(email));

        }
        log.info(speakerService.showList().toString());

        return "suggested_topics";
    }
    @PostMapping("/save_speaker")
    public String saveSpeaker(@RequestParam(name = "name", required = false) String name,
                             @RequestParam(name = "topic_for_speaker", required = false) String topic_for_speaker){

        log.info("Username: {}, Topic for speaker: {}", name, topic_for_speaker);

        Speaker speaker = new Speaker();
        speaker.setEmail(name);
        speaker.setTopic_for_speaker(topic_for_speaker);

        speakerService.save(speaker);

        return "moderator_account";
    }

}
