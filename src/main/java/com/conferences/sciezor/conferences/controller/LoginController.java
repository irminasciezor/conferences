package com.conferences.sciezor.conferences.controller;
import com.conferences.sciezor.conferences.repository.UserRepository;
import com.conferences.sciezor.conferences.service.SecurityService;
import com.conferences.sciezor.conferences.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {
    @Autowired
    UserRepository userRepository;

    @Autowired
    private SecurityService securityService;

    @Autowired
    UserService userService;

    @GetMapping("/login")
    public ModelAndView login(String error, String logout) {
        ModelAndView model = new ModelAndView();
        if (securityService.isAuthenticated()) {
            model.setViewName("index");
        }
        if (error != null)
            model.addObject("error", "Your username and password is invalid.");

        if (logout != null)
            model.addObject("message", "You have been logged out successfully.");

        model.setViewName("login");

        return model;
    }


}

