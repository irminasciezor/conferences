package com.conferences.sciezor.conferences.controller;
import com.conferences.sciezor.conferences.entity.SpeakerEvent;
import com.conferences.sciezor.conferences.entity.User;
import com.conferences.sciezor.conferences.service.impl.SpeakerEventServiceImpl;
import com.conferences.sciezor.conferences.service.impl.SpeakerServiceImpl;
import com.conferences.sciezor.conferences.service.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SpeakerController {

    @Autowired
    SpeakerServiceImpl speakerService;

    @Autowired
    SpeakerEventServiceImpl speakerEventService;

    @Autowired
    UserServiceImpl userService;

    private static final Logger log = LoggerFactory.getLogger(SpeakerController.class);

    @GetMapping("/speaker_account")
    @PreAuthorize("hasRole('SPEAKER')")
    public ModelAndView speakerAccount(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("speaker_account");

        return modelAndView;
    }

    @GetMapping("/participated_events")
    public String showAllTopicsForSpeakers(Model model){

        final String currentLogin = SecurityContextHolder.getContext().getAuthentication().getName();

        User user = userService.getUser(currentLogin);
        if(user != null) {
            model.addAttribute("suggestions", speakerEventService.findAllEventsFilter(user.getUsername()));
        }
        log.info(speakerEventService.findAllEventsFilter(user.getUsername()).toString());

        return "participated_events";
    }

    @PostMapping("/save_speaker_event")
    public String saveSpeaker(@RequestParam(name = "nameOfSpeaker", required = false) String nameOfSpeaker,
                              @RequestParam(name = "nameOfEvent", required = false) String nameOfEvent,
                              @RequestParam(name = "topicOfReport", required = false) String topicOfReport){

        log.info("Name of speaker: {}, Name of event: {}, Topic for report: {}", nameOfSpeaker, nameOfEvent, topicOfReport);

        SpeakerEvent speakerEvent = new SpeakerEvent();
        speakerEvent.setNameOfSpeaker(nameOfSpeaker);
        speakerEvent.setNameOfEvent(nameOfEvent);
        speakerEvent.setTopicOfReport(topicOfReport);

        speakerEventService.save(speakerEvent);

        return "speaker_account";
    }
}
