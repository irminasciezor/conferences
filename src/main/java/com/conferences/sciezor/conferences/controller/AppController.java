package com.conferences.sciezor.conferences.controller;
import com.conferences.sciezor.conferences.dto.UserDTO;
import com.conferences.sciezor.conferences.entity.Roles;
import com.conferences.sciezor.conferences.repository.RoleRepository;
import com.conferences.sciezor.conferences.service.exception.UserAlreadyExistException;
import com.conferences.sciezor.conferences.service.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@Controller
public class AppController {

    @Autowired
    RoleRepository roleRepository;

    private static final Logger log = LoggerFactory.getLogger(AppController.class);

    private final UserServiceImpl userService;


    public AppController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {

        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
        webDataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
    }

    @GetMapping("/register")
    public String register(@ModelAttribute UserDTO userDTO, Model model) {

        model.addAttribute("userDTO", userDTO);
//        roleRepository.saveRoles();
        List<Roles> roles =roleRepository.findAll();
        model.addAttribute("allRoles", roles);

        return "register";
    }

    @GetMapping("/index")
    public String home(){
        return "index";
    }

    @PostMapping("/register")
    public String save(@Valid UserDTO userDTO, BindingResult bindingResult,
                       @RequestParam(name = "idroles")Integer id) throws UserAlreadyExistException {

        log.info("Role id = "+ id);

            if (bindingResult.hasErrors()) {
                log.info("registration not possible");
                return "register";
            }

            log.info("after registration");

            userService.register(userDTO);

            return "register_success";
    }


}
