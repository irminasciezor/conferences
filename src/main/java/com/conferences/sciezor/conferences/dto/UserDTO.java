package com.conferences.sciezor.conferences.dto;

import com.conferences.sciezor.conferences.entity.User;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.stereotype.Controller;

//@PasswordMatches
@Controller
@Getter
@Setter
@ToString
public class UserDTO {
    private Long id;
    private String email;
    private String username;
    private String password;
    private String repeatPassword;
    private String name;
    private String surname;
    private String country;
    private String city;
    private String phoneNumber;
    private String formError;

    public UserDTO() {
    }

    public UserDTO(String email, String password, String repeatPassword, String name, String surname, String country, String city, String phoneNumber, String username) {
        this.email = email;
        this.password = password;
        this.repeatPassword = repeatPassword;
        this.name = name;
        this.surname = surname;
        this.country = country;
        this.city = city;
        this.phoneNumber = phoneNumber;
        this.username = username;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", repeatPassword='" + repeatPassword + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", formError='" + formError + '\'' +
                '}';
    }
}
