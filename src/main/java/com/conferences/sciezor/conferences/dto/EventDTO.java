package com.conferences.sciezor.conferences.dto;
import com.conferences.sciezor.conferences.entity.Event;
import java.sql.Time;
import java.util.Date;
import java.util.List;

public class EventDTO {
    private List<Event> events;

    private Long id;
    private String name;

    private Date date; //String

    private int participants;

    private int reports;

    private String place;

    private Time time; //String


    public void addEvent(Event event){
        this.events.add(event);
    }

    public EventDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EventDTO(Long id, List<Event> events, String name, Date date, int participants, int reports, String place, Time time) {
        this.id = id;
        this.events = events;
        this.name = name;
        this.date = date;
        this.participants = participants;
        this.reports = reports;
        this.place = place;
        this.time = time;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getParticipants() {
        return participants;
    }

    public void setParticipants(int participants) {
        this.participants = participants;
    }

    public int getReports() {
        return reports;
    }

    public void setReports(int reports) {
        this.reports = reports;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }
}
