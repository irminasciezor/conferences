package com.conferences.sciezor.conferences.dto;

public class ReportDTO {

    private Long id;

    private String topic;

    public ReportDTO(Long id, String topic) {
        this.id = id;
        this.topic = topic;
    }

    public ReportDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }
}
