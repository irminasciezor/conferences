INSERT INTO `roles` (`idroles`,`nameofrole`) VALUES (1,'USER');
INSERT INTO `roles` (`idroles`,`nameofrole`) VALUES (2,'MODERATOR');
INSERT INTO `roles` (`idroles`,`nameofrole`) VALUES (3,'SPEAKER');

INSERT INTO `con_user` (`iduser`,`city`, `country`, `email`, `name`, `password`, `phone`, `surname`, `username`)
VALUES ('50','a','aa','a@a.pl','aaa','a','123456','aaaa','a1');
INSERT INTO `con_user` (`city`, `country`, `email`, `name`, `password`, `phone`, `surname`, `username`)
VALUES ('b','bb','b@b.pl','bbb','b','123456','bbbb','b1');
INSERT INTO `con_user` (`city`, `country`, `email`, `name`, `password`, `phone`, `surname`, `username`)
VALUES ('c','cc','c@c.pl','ccc','c','123456','cccc','c1');
INSERT INTO `con_user` (`city`, `country`, `email`, `name`, `password`, `phone`, `surname`, `username`)
VALUES ('l','ll','l@l','lll','l','123456','llll','l1');

INSERT INTO `users_roles` (`user_id`, `idroles`) VALUES ('41', '1');
INSERT INTO `users_roles` (`user_id`, `idroles`) VALUES ('42', '2');
INSERT INTO `users_roles` (`user_id`, `idroles`) VALUES ('43', '3');
INSERT INTO `users_roles` (`user_id`, `idroles`) VALUES ('50', '2');