CREATE TABLE authorities (
                               username varchar(45) NOT NULL,
                               authority varchar(45) NOT NULL,
                               id int NOT NULL,
                               PRIMARY KEY (id)
);

CREATE TABLE `event` (
                         `idevent` int NOT NULL AUTO_INCREMENT,
                         `date` datetime(6) DEFAULT NULL,
                         `name` varchar(255) DEFAULT NULL,
                         `participants` int DEFAULT NULL,
                         `place` varchar(255) DEFAULT NULL,
                         `reports` int DEFAULT NULL,
                         `time` time DEFAULT NULL,
                         `user_iduser` int NOT NULL,
                         `actual_participants` int NOT NULL DEFAULT '0',
                         PRIMARY KEY (`idevent`),
                         KEY `fk_event_iduser` (`user_iduser`),
                         KEY `indexx` (`name`),
                         CONSTRAINT `fk_event_iduser` FOREIGN KEY (`user_iduser`) REFERENCES `con_user` (`iduser`)
);
CREATE TABLE `register` (
                            `idregistration` int NOT NULL,
                            `user_email` varchar(255) DEFAULT NULL,
                            `event_name` varchar(255) DEFAULT NULL,
                            PRIMARY KEY (`idregistration`),
                            KEY `fk_user_email_idx` (`user_email`),
                            KEY `fk_event_name_idx` (`event_name`),
                            CONSTRAINT `fk_event_name` FOREIGN KEY (`event_name`) REFERENCES `event` (`name`),
                            CONSTRAINT `fk_user_email` FOREIGN KEY (`user_email`) REFERENCES `con_user` (`email`)
);
CREATE TABLE report (
                          idreport bigint NOT NULL,
                          topic varchar(255) DEFAULT NULL,
                          PRIMARY KEY (idreport)
);
CREATE TABLE roles (
                         idroles bigint NOT NULL AUTO_INCREMENT,
                         nameofrole varchar(255) DEFAULT NULL,
                         PRIMARY KEY (idroles)
);
CREATE TABLE con_user (
                        iduser int NOT NULL AUTO_INCREMENT,
                        city varchar(255) DEFAULT NULL,
                        country varchar(255) DEFAULT NULL,
                        email varchar(255) DEFAULT NULL,
                        name_of_user varchar(255) DEFAULT NULL,
                        password_user varchar(255) DEFAULT NULL,
                        phone varchar(255) DEFAULT NULL,
                        surname varchar(255) DEFAULT NULL,
                        username varchar(255) DEFAULT NULL,
                        roles_idroles bigint DEFAULT NULL,
                        id_user int DEFAULT NULL,
                        PRIMARY KEY (iduser)
);
CREATE TABLE users (
                         username varchar(45) NOT NULL,
                         password_of_user varchar(80) NOT NULL,
                         enabled tinyint(1) DEFAULT 1,
                         PRIMARY KEY (username)
);
CREATE TABLE users_roles (
                               iduser int NOT NULL,
                               idroles int NOT NULL,
                               KEY user_fk_idx (iduser),
                               KEY role_fk_idx (idroles),
                               CONSTRAINT roles_fk FOREIGN KEY (idroles) REFERENCES roles (idroles),
                               CONSTRAINT user_fk FOREIGN KEY (iduser) REFERENCES con_user (iduser)
);
CREATE TABLE `speaker_topics` (
                                  `idspeaker_topics` int NOT NULL,
                                  `email_of_the_speaker` varchar(45) NOT NULL,
                                  `suggested_topic` varchar(45) NOT NULL,
                                  `idspeaker` int,
                                  PRIMARY KEY (`idspeaker_topics`),
                                  KEY `fk_email_speaker_idx` (`email_of_the_speaker`),
                                  CONSTRAINT `fk_email_speaker` FOREIGN KEY (`email_of_the_speaker`) REFERENCES `con_user` (`email`)
);
CREATE TABLE `speaker_events` (
                                  `idspeaker_events` int NOT NULL,
                                  `name_of_speaker` varchar(45) NOT NULL,
                                  `name_of_event` varchar(45) NOT NULL,
                                  `topic_of_report` varchar(45) NOT NULL,
                                  PRIMARY KEY (`idspeaker_events`),
                                  KEY `fk_name_of_event_idx` (`name_of_event`),
                                  CONSTRAINT `fk_name_of_event` FOREIGN KEY (`name_of_event`) REFERENCES `event` (`name`)
);


